#!/bin/bash

sqlplus tysonprod/Pragma1234@tysonbkup.cfkgpgedasgz.us-east-1.rds.amazonaws.com:1521/BKUPDB <<EOF 
whenever sqlerror exit sql.sqlcode;
set echo off
set heading off

set timing on

EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_OCR_ROUTING');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_OCR_ROUTING_DEFAULT');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_OCR_VALIDATION_RULES');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_OCR_VALIDATION_VALUES');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_OCR_ACTIVITY_HISTORY');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_POOLING_INTERVAL');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_CORRELATION_NAMES');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_AWS_S3');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_SFG_CD');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_PEM_DB_OBJECTS');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_PEM_PARTNER_CODES');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_SERVICE_RUN');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_CD');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_PEM_TEMP');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_PEM_DB_OBJ_DATA_TYPES');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_RULES_STAGING');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_SFG_CUSTOMPROTOCOL');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_TRANSINFOD_STAGING');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_TRANSFERINFO_SCH_CONF');
EXEC DBMS_STATS.gather_table_stats('TYSON_PRD','PETPE_TRANSFERINFO_STAGING');

EOF