variable "user" {
   description = "server user details"
   type        = string
}

variable "host" {
   description = "server host details"
   type        = string
}

variable "key" {
   description = "server key details"
   type        = any
}

variable "script" {
   description = "server script details"
   type        = any
}