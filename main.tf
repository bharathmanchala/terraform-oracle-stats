 resource "null_resource" "testing"{

  provisioner "remote-exec" {
       connection {
        #  Connect to server
        #  connect to aws s3
        #  copy files from s3
        #  Paste the files to server path
        #  Exits from the server
    type     = "ssh"
    user     = var.user
    host     = var.host
    # password = var.password   
    private_key = file(var.key)
  }
     script   = (var.script)
  } 
}